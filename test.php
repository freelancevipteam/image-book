<?php
require 'vendor/autoload.php';
require 'functions.php';

use Intervention\Image\ImageManager;

$manager = new ImageManager(array('driver' => 'imagick'));

$path = 'source/img_7003.jpg'; // сюда вписать путь к исходному файлу

generate($manager, $path, 'out.png', 3, 2, true);
