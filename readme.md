# Скрипт генерации 3Д обложки книг

**Установка**

- `git clone https://freelancevip@bitbucket.org/freelancevipteam/image-book.git`

- `composer install`

**Требования**

- PHP 7.2
- ImageMagic

Инструкция

Исходные изображения находятся в папке source. Готовые будут в папке result. В файле books-processing.csv должны быть
указаны данные

`original,result,paper,cover`

Для запуска ввести

`php run.php`

![Alt text](out.png?raw=true "Пример")