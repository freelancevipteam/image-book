<?php

require 'vendor/autoload.php';
require 'functions.php';

use Intervention\Image\ImageManager;

$manager = new ImageManager(array('driver' => 'imagick'));

$data = csv2array('books-processing.csv');

file_put_contents('check_this_files.txt', '');

foreach ($data as $index => $item) {
    $source_path = 'source/' . $item['original'];
    $dest_path   = 'result/' . $item['result'];
    try {
        echo "Generate #{$index}: {$item['original']}" . PHP_EOL;
        generate($manager, $source_path, $dest_path, $item['paper'], $item['cover']);
    } catch (Exception $e) {
        echo $e->getMessage() . PHP_EOL;
    }
}
