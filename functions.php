<?php

use Intervention\Image\AbstractShape;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;

/**
 * @param  ImageManager  $manager
 * @param                $source_path string
 * @param                $dest_path   string
 * @param                $paper_width int (%)
 * @param                $cover_width int (%)
 * @param  bool          $trim
 */
function generate(
    ImageManager $manager,
    $source_path,
    $dest_path,
    $paper_width,
    $cover_width,
    $trim = true
) {
    $settings = parse_ini_file("settings.ini");

    $image = $manager->make($source_path);
    if ($trim) {
        trimBorders($image, $source_path);
    }
    $width  = $image->width();
    $height = $image->height();

    $averageColor = getColorAverage($image);

    $delta_w = intval($width * ($paper_width + $cover_width) / 100);

    $canvas = $manager->canvas($width + $delta_w, $height);
    $canvas->fill(array(0, 0, 0, 0));

    // White vertical line on the left side
    $line_width = intval($width * 3 / 560);
    $image->rectangle(
        intval($width * 20 / 560),
        0,
        intval($width * 19 / 560 + $line_width),
        $height,
        function (AbstractShape $draw) {
            $draw->background(array(255, 255, 255, 0.3));
        }
    );

    // Transform - Distort
    $im = $image->getCore();
    $im->setImageFormat('png');
    $im->setImageVirtualPixelMethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
    $im->setImageMatte(true);
    $distortVal    = $settings['distort_value'];
    $controlPoints = array(
        0,
        0,
        0,
        intval($distortVal * $height), # top left
        $width,
        0,
        $width,
        0, # top right
        $width,
        $height,
        $width,
        $height, # bottom right
        0,
        $height,
        0,
        $height - intval($distortVal * $height) # bottum left
    );
    $im->distortImage(Imagick::DISTORTION_PERSPECTIVE, $controlPoints, false);
    $image = $manager->make($im);

    $canvas->insert($image);

    // Подложка
    $substrate_top    = intval($height * 11 / 870);
    $substrate_bottom = $height - intval($height * 15 / 870);
    $canvas->rectangle(
        $width,
        $substrate_top,
        $width + $delta_w,
        $substrate_bottom,
        function (AbstractShape $draw) use ($averageColor) {
            $draw->background($averageColor);
        }
    );

    // Правый угол подложки
    $points = [
        $width,
        $substrate_bottom,
        $width + $delta_w,
        $substrate_bottom,
        $width,
        $substrate_bottom - intval(
            $height * ($cover_width + $paper_width) * 1.75 / 100
        ),
    ];
    $canvas->polygon(
        $points,
        function (AbstractShape $draw) {
            $draw->background('#2c3f4d');
        }
    );

    // Paper
    $canvas->rectangle(
        $width,
        $substrate_top + intval($width * $cover_width / 100),
        $width + intval($width * $paper_width / 100),
        $substrate_bottom - intval($width * $cover_width / 100),
        function (AbstractShape $draw) {
            $draw->background('#fff');
        }
    );

    // Scale
    if (isset($settings['scale_result_image'])) {
        $canvas->widen(
            $settings['output_image_max_width'],
            function ($constraint) {
                $constraint->upsize();
            }
        );
    }

    $canvas->save($dest_path);
}

/**
 * Remove white borders
 *
 * @param  Image   $image
 * @param  string  $source_path
 *
 * @return Image
 */
function trimBorders(Image $image, $source_path)
{
    $height        = $image->height();
    $width         = $image->width();
    $top_border    = 0;
    $bottom_border = 0;

    $y = 1;

    $min_color = 240;

    $max_y       = intval($height * 0.1);
    $max_borders = false;

    do {
        $top_border++;
        $pixel_color = $image->pickColor(0, $y);
        $y++;
    } while ($y <= $max_y
    && ($pixel_color[0] > $min_color
        && $pixel_color[1] > $min_color
        && $pixel_color[2] > $min_color));

    if ($y == $max_y + 1) {
        $max_borders = true;
    }

    $y = 2;
    do {
        $bottom_border++;
        $pixel_color = $image->pickColor(0, $height - $y);
        $y++;
    } while ($y <= $max_y
    && ($pixel_color[0] > $min_color
        && $pixel_color[1] > $min_color
        && $pixel_color[2] > $min_color));

    if ($y == $max_y + 1) {
        $max_borders = true;
    }

    if ($max_borders) {
        file_put_contents(
            'check_this_files.txt',
            $source_path.PHP_EOL,
            FILE_APPEND
        );
    } else {
        if ($top_border || $bottom_border) {
            $image->crop(
                $width,
                $height - $top_border - $bottom_border,
                0,
                $top_border
            );
        }
    }

    return $image;
}

/**
 * @param  Image  $image
 *
 * @return string
 */
function getColorAverage(Image $image)
{
    $image = clone $image;
    $color = $image->limitColors(1)->pickColor(0, 0);
    $image->destroy();

    return $color;
}

/**
 * @param $file_name string
 *
 * @return array
 */
function csv2array($file_name)
{
    $data = $header = array();
    $i    = 0;
    $file = fopen($file_name, 'r');
    while (($line = fgetcsv($file)) !== false) {
        if ($i == 0) {
            $header = $line;
        } else {
            $data[] = $line;
        }
        $i++;
    }
    fclose($file);
    $_data = [];
    foreach ($data as $_value) {
        $new_item = array();
        foreach ($_value as $key => $value) {
            $new_item[$header[$key]] = $value;
        }
        $_data[] = $new_item;
    }

    return $_data;
}